/**
 * 
 */
package om.algorithms.ds;

import java.util.Collection;

/**
 * @author john
 *
 */
public class PriorityQueue<T extends Object & PriorityQueueElement & Comparable<? super T>> extends Heap<T> {
  public PriorityQueue() {
    this(true);
  }
  
  public PriorityQueue(boolean maxPriority) {
    super(maxPriority);
  }
  
  public PriorityQueue(T[] elements) {
    this(elements, true);
  }
  
  public PriorityQueue(T[] elements, boolean maxPriority) {
    super(elements, maxPriority);
  }
  
  public PriorityQueue(Collection<T> elements) {
    this(elements, true);
  }
  
  public PriorityQueue(Collection<T> elements, boolean maxPriority) {
    super(elements, maxPriority);
  }
  
  public void push(T element) {
    insert(element);
  }
  
  public T pop() {
    return removeRoot();
  }
  
  public void updatePriority(int index, int value) {
    T element = get(index);
    
    if(element != null) {
      element.updatePriority(value);
      if(value > 0) {
        if(isMaxHeap()) {
          correctAfterNodeUpdate(index);
        }
        else { 
          heapify(index);
        }
      }
      else {
        if(isMaxHeap()) {
          heapify(index);
        }
        else {
          correctAfterNodeUpdate(index);
        }
      }
    }
  }
      
}
