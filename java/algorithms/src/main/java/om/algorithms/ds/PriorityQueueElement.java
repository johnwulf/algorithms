/**
 * 
 */
package om.algorithms.ds;

/**
 * @author john
 *
 */
public interface PriorityQueueElement {
  public void updatePriority(int value);
}
