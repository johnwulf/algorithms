/**
 * 
 */
package om.algorithms.ds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author john
 *
 */
public class Heap<T extends Object & Comparable<? super T>> {
  
  private ArrayList<T> heap;
  private boolean maxHeap;
  private int begin;
  private int end;
  
  public Heap(boolean maxHeap) {
    this.maxHeap = maxHeap;
    heap = new ArrayList<T>();
    begin = end = 0;
  }
   
  public Heap() {
    this(true);
  }
  
  public Heap(Collection<T> elements, boolean maxHeap) {
    this.maxHeap = maxHeap;
    heap = new ArrayList<T>(elements);
    begin = 0;
    end = heap.size();
    buildHeap();
  }
  
  public Heap(T[] elements, boolean maxHeap) {
    this(Arrays.asList(elements), maxHeap);
  }
  
  public boolean isMaxHeap() {
    return maxHeap;
  }
  
  /**
   * Returns the element at the given node. The nodes are numbered
   * from 1 to <code>heap.size()</code>, with the root being 1 and 
   * <code>heap.size()</code> being the right most leave of the heap.
   * 
   * @param index the index of the node to be returned.
   * @return the element at <code>index</code>
   */
  public T get(int index) {
    return heap.get(begin + index - 1);
  }
  
  /**
   * Sets the node for the given index to the given element.
   * The nodes are numbered from 1 to <code>heap.size()</code>, with the root being 1 and 
   * <code>heap.size()</code> being the right most leave of the heap. The 
   * method returns the element that have been replaced.
   * 
   * @param index the index of the node to be returned.
   * @param element the element to which set the node at the given index
   * 
   * @return the element that was replaced at <code>index</code>
   */
  private T set(int index, T element) {
    T tmp = null;
    if((index > 0) && (index <= size())) {
      tmp = heap.get(begin + index - 1);
      heap.set(begin + index - 1, element);
    }
    
    return tmp;
  }

  /**
   * Inserts a new element into the heap by adding it as a new leaf and 
   * bubbling the new element up until it is at its right position.
   * <br>
   * Running time: O(log(n))
   *  
   * @param element the element to insert into the heap
   */
  public void insert(T element) {
    heap.add(end++, element);
    correctAfterNodeUpdate(end);
  }
  
  protected void correctAfterNodeUpdate(int index) {
    int parent = parentIndex(index);
    boolean done = false;
    
    while((parent > 0) && !done) {
      if(maxHeap) {
        done = get(parent).compareTo(get(index)) >= 0;
      }
      else {
        done = get(parent).compareTo(get(index)) <= 0;
      }
      if(!done) {
        set(parent, set(index, get(parent)));
      }
      index = parent;
      parent = parentIndex(index);
    }
  }
    
  /**
   * Removes the root from the heap and applies heapify to reestablish the 
   * heap property.
   *  
   * 
   * @return the root of the heap, which is either the largest (max heap) or 
   *         the smallest (min heap) element.
   */
  public T removeRoot() {
    T root = null;
    if(size() > 0) {
      root = set(1, get(size()));
      --end;
      heap.remove(heap.size() - 1);
      heapify(1);
    }
    
    return root;
  }
 
  /**
   * Switches the root with the last leaf, reduces the heap size by one, and
   * applies heapify to reestablish the heap property. Even though the heap 
   * size is reduced by one, the size of the underlying array is not changed,
   * thus enabling to execute a heap sort, by simply calling this method 
   * until the heap size is equal to one, and getting the sorted array by 
   * calling <code>toArrayAll</code>. 
   * 
   * @see #toArrayAll(Object[])
   */
  public void switchRoot() {
    if(size() > 1) {
      set(size(), set(1, get(size())));
      --end;
      heapify(1);
    }
  }
   
      
  public int size() {
    return end - begin;
  }
  
  /**
   * Returns the index of the left child for the node with the
   * given <code>index</code>. 
   * @param index the index for which the left child index is to be calculated
   * @return <ul>
   *            <li>the index of the left child if the node for the given index has 
   *                has a left child
   *            <li>0 if the element is a leaf
   *            <li>-1 if the index does not point to a valid node
   *         </ul> 
   * @see #childNodeIndex(int, boolean)        
   */
  public int leftChildIndex(int index) {
    return childNodeIndex(index, true);
  }
  
  /**
   * Returns the index of the ight child for the node with the
   * given <code>index</code>. 
   * @param index the index for which the right child index is to be calculated
   * @return <ul>
   *            <li>the index of the right child if the node for the given index has 
   *                has a right child
   *            <li>0 if the element at the given index does not have a right child
   *            <li>-1 if the index does not point to a valid node
   *         </ul> 
   * @see #childNodeIndex(int, boolean)        
   */
  public int rightChildIndex(int index) {
    return childNodeIndex(index, false);
  }

  /**
   * Returns the index of the parent for the given <code>index</code>
   * @param index the index for which the parent index is to be calculated
   * @return <ul>
   *            <li>the index of the parent if the node for the given index has 
   *                has a parent
   *            <li>0 if the given index is the root (the only node without a parent)
   *            <li>-1 if the index does not point to a valid node
   *         </ul> 
   *         
   */
  public int parentIndex(int index) {
    int parent = -1;
    
    if((index <= size()) && (index > 0)) {
      if(index > 0) {
        parent = index >> 1;
      }
      else {
        parent = 0;
      }
    }
    return parent;
  }
  
  public <E> E[] toArray(E[] a) {
    ArrayList<T> tmp;
    if((end - begin) < heap.size()) {
      tmp = new ArrayList<T>(heap);
      for(int i = 0; i < begin; ++i) {
        tmp.remove(0);
      }
      
      while((end - begin) < tmp.size()) {
        tmp.remove(tmp.size() - 1);
      }
    }
    else {
      tmp = heap;
    }
      
    return tmp.toArray(a);
  }
  
  public <E> E[] toArrayAll(E[] a) {
    return heap.toArray(a);
  }
  
  private void buildHeap() {
    for(int i = size() >> 1; i > 0; --i) {
      heapify(i);
    }
  }
  
  /**
   * Maintains the heap property on the sub-heap of the node with the 
   * specified index. The assumption is that the left and right subtrees
   * are proper heaps.
   * 
   * @param index the index of the node to heapify
   * @return <code>true</code> if the tree was already a heap;<code>fales</code>
   *         otherwise
   */
  protected boolean heapify(int index) {
    boolean isHeap = true;
    int parent, left, right;
    if((index > 0) && (index <= size())) {
      parent = index;
      left = leftChildIndex(index);
      right = rightChildIndex(index);
      if(maxHeap) {
        if((left > 0) && (get(parent).compareTo(get(left)) < 0)) {
          parent = left;
        }
        if((right > 0) && (get(parent).compareTo(get(right)) < 0)) {
          parent = right;
        }
      }
      else {
        if((left > 0) && (get(parent).compareTo(get(left)) > 0)) {
          parent = left;
        }
        if((right > 0) && (get(parent).compareTo(get(right)) > 0)) {
          parent = right;
        }
      }
      
      if(parent != index) {
        isHeap = false;
        set(parent, set(index, get(parent)));
        heapify(parent);
      }
    }
    
    return isHeap;
  }
  
  /**
   * Returns the index of the left or right child for the node with the
   * given <code>index</code>. 
   * @param index the index for which the parent index is to be calculated
   * @param left <code>true</code> if the index for the left child is to be calculated;
   *             <code>false</code> if the index for the right child is to be calculated
   * @return <ul>
   *            <li>the index of the left or right child if the node for the given index has 
   *                has a parent
   *            <li>0 if the given index does not have the requested child  
   *            <li>-1 if the index does not point to a valid node
   *         </ul> 
   *         
   */
  private int childNodeIndex(int index, boolean left) {
    int child = -1;
    
    if((index <= size()) && (index > 0)) {
      child = (index << 1) + (left ? 0 : 1);
      if(child > size()) {
        child = 0;
      }
    }
    return child;
  }
}
