/**
 * 
 */
package om.algorithms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
 * @author john
 *
 */
public class Sorting {
  /**
   * Sorts the specified collection in ascending order using bubble sort.
   * @param col collection to be sorted
   * @return a sorted copy of the specified collection.
   * 
   * @see #bubbleSort(Collection, Comparator)
   */
  public static <T extends Object & Comparable<? super T>> ArrayList<T> bubbleSort(Collection<? extends T> col) {
    return bubbleSort(col, new Comparator<T>() {
      public int compare(T a, T b) { return a.compareTo(b); }
      public boolean equals(T a, T b) { return a.equals(b); }
    });
    

  }
  
  /**
   * Sorts the specified collection in ascending order using bubble sort with
   * the provided comparator. The sort will create a copy of the collection. The original collection will
   * not be modified.
   * <p>
   * <ul>
   * <li>Time complexity: O(n<sup>2</sup>)
   * <li>Space complexity: O(1)
   * </ul>
   * <p>
   * For more information see <a href="http://en.wikipedia.org/wiki/Bubble_sort">Bubble Sort</a>
   *
   * @param col the collection to be sorted
   * @param comp the comparator to be used. 
   * 
   * @return a sorted copy of the provided collection
   */
  public static <T> ArrayList<T> bubbleSort(Collection<? extends T> col, Comparator<? super T> comp) {
    ArrayList<T> sortedList = new ArrayList<T>(col);

    for(int i = 0; i < sortedList.size() - 1; ++i) {
      for(int j = 0; j < sortedList.size() - 1 - i; ++j) {
        if(comp.compare(sortedList.get(j), sortedList.get(j + 1)) > 0) {
          sortedList.set(j, sortedList.set(j + 1, sortedList.get(j)));
        }
      }
    }
    
    return sortedList;
  }
  
  /**
   * Sorts the specified collection in ascending order using selection sort.
   * @param col collection to be sorted
   * @return a sorted copy of the specified collection.
   * 
   * @see #selectionSort(Collection, Comparator)
   */
  public static <T extends Object & Comparable<? super T>> ArrayList<T> selectionSort(Collection<? extends T> col) {
    return selectionSort(col, new Comparator<T>() {
      public int compare(T a, T b) { return a.compareTo(b); }
      public boolean equals(T a, T b) { return a.equals(b); }
    });
  }
  
  /**
   * Sorts the specified collection in ascending order using selection sort with
   * the provided comparator. The sort will create a copy of the collection. The original collection will
   * not be modified.
   * <p>
   * <ul>
   * <li>Time complexity: O(n<sup>2</sup>)
   * <li>Space complexity: O(1)
   * </ul>
   * <p>
   * For more information see <a href="http://en.wikipedia.org/wiki/Selection_sort">Selection Sort</a>
   *
   * @param col the collection to be sorted
   * @param comp the comparator to be used. 
   * 
   * @return a sorted copy of the provided collection
   */
   public static <T> ArrayList<T> selectionSort(Collection<? extends T> col, Comparator<? super T> comp) {
    ArrayList<T> sortedList = new ArrayList<T>(col);
    int maxIndex = 0;
    
    for(int i = 0; i < sortedList.size() - 1; ++i) {
      maxIndex = i;
      for(int j = i + 1;j < sortedList.size(); ++ j) {
        if(comp.compare(sortedList.get(maxIndex), sortedList.get(j)) > 0) {
          maxIndex = j;
        }
      }
      if(maxIndex != i) {
        sortedList.set(i, sortedList.set(maxIndex, sortedList.get(i)));
      }
      
    }
    
    return sortedList;
  }
 
  /**
   * Sorts the specified collection in ascending order using insertion sort with only 
   * one insertion per element.
   * 
   * @param col collection to be sorted
   * @return a sorted copy of the specified collection.
   * 
   * @see #insertionSort(Collection, Comparator)
   */
  public static <T extends Object & Comparable<? super T>> ArrayList<T> insertionSort(Collection<? extends T> col) {
    return insertionSort(col, new Comparator<T> () {
      public int compare(T a, T b) { return a.compareTo(b); }
      public boolean equals(T a, T b) { return a.equals(b); }
    });
  }
  
  /**
   * Sorts the specified collection in ascending order using insertion sort with
   * the provided comparator. The sort will create a copy of the collection. The original collection will
   * not be modified.
   * <p>
   * <ul>
   * <li>Time complexity: O(n<sup>2</sup>)
   * <li>Space complexity: O(1)
   * </ul>
   * <p>
   * For more information see <a href="http://en.wikipedia.org/wiki/Insertion_sort">Insertion Sort</a>
   *
   * @param col the collection to be sorted
   * @param comp the comparator to be used. 
   * 
   * @return a sorted copy of the provided collection
   */
   public static <T> ArrayList<T> insertionSort(Collection<? extends T> col, Comparator<? super T> comp) {
    ArrayList<T> sortedList = new ArrayList<T>(col);
    
    for(int i = 1; i < sortedList.size(); ++i) {
      for(int j = 0; j < i; ++j) {
        if(comp.compare(sortedList.get(j), sortedList.get(i)) > 0) {
          sortedList.add(j, sortedList.remove(i));
          break;
        }
      }
    }
    
    return sortedList;
  }
  
  /**
   * Sorts the specified collection in ascending order using insertion sort.
   * @param col collection to be sorted
   * @return a sorted copy of the specified collection.
   * 
   * @see #insertionSort(Collection, Comparator)
   */
  public static <T extends Object & Comparable<? super T>> ArrayList<T> insertionSort2(Collection<? extends T> col) {
    return insertionSort2(col, new Comparator<T>() {
      public int compare(T a, T b) { return a.compareTo(b); }
      public boolean equals(T a, T b) { return a.equals(b); }
    });
  }
  
  /**
   * Sorts the specified collection in ascending order using insertion sort with
   * the provided comparator. The sort will create a copy of the collection. The original collection will
   * not be modified.
   * <p>
   * <ul>
   * <li>Time complexity: O(n<sup>2</sup>)
   * <li>Space complexity: O(1)
   * </ul>
   * <p>
   * For more information see <a href="http://en.wikipedia.org/wiki/Insertion_sort">Insertion Sort</a>
   *
   * @param col the collection to be sorted
   * @param comp the comparator to be used. 
   * 
   * @return a sorted copy of the provided collection
   */
   public static <T> ArrayList<T> insertionSort2(Collection<? extends T> col, Comparator<? super T> comp) {
    ArrayList<T> sortedList = new ArrayList<T>(col);
    
    for(int i = 1; i < sortedList.size(); ++i) {
      for(int j = i - 1; j >= 0; --j) {
        if(comp.compare(sortedList.get(j), sortedList.get(j + 1)) > 0) {
          sortedList.set(j, sortedList.set(j + 1, sortedList.get(j)));
        }
        else {
          break;
        }
      }
    }
    
    return sortedList;
  }
   
  /**
   * Sorts the specified collection in ascending order using merge sort.
   * @param col collection to be sorted
   * @return a sorted copy of the specified collection.
   * 
   * @see #mergeSort(Collection, Comparator)
   */
  public static <T extends Object & Comparable<? super T>> ArrayList<T> mergeSort(Collection<? extends T> col) {
    return mergeSort(col, new Comparator<T>() {
      public int compare(T a, T b) { return a.compareTo(b); }
      public boolean equals(T a, T b) { return a.equals(b); }
    });
  }

  /**
   * Sorts the specified collection in ascending order using merge sort with
   * the provided comparator. The sort will create a copy of the collection. The original collection will
   * not be modified. <br>
   * The algorithm uses a divide and conquer strategy by dividing the array into half and apply merge sort
   * recursively. Hence, the stack will grow to log n as part of the sort.
   * <p>
   * <ul>
   * <li>Time complexity: O(n log n)
   * <li>Space complexity: O(n)
   * </ul>
   * <p>
   * For more information see <a href="http://en.wikipedia.org/wiki/Merge_sort">Merge Sort</a>
   *
   * @param col the collection to be sorted
   * @param comp the comparator to be used. 
   * 
   * @return a sorted copy of the provided collection
   */
   public static <T> ArrayList<T> mergeSort(Collection<? extends T> col, Comparator<? super T> comp) {
    return mergeSortRecursion(new ArrayList<T>(col), 0, col.size(), comp);
  }
  
  private static <T> ArrayList<T> mergeSortRecursion(ArrayList<? extends T> list, int start, int n, Comparator<? super T> comp) {
    ArrayList<T> left, right, sorted;
    
    if(n > 1) {
      left = mergeSortRecursion(list, start , n >> 1, comp);
      right = mergeSortRecursion(list, start + (n >> 1), n - (n >> 1), comp);
      sorted = mergeInPlace(left, right, comp);
    }
    else {
      sorted = new ArrayList<T>(1);
      sorted.add(list.get(start));
    }
    
    return sorted;
  }
  
  private static <T> ArrayList<T> mergeInPlace(ArrayList<? extends T> a, ArrayList<? extends T> b, Comparator<? super T> comp) {
    ArrayList<T> merged = new ArrayList<T>(a.size() + b.size());
    int n = 0, m = 0;
    
    while((n < a.size()) && (m < b.size())) {
      if(comp.compare(a.get(n), b.get(m)) > 0) {
        merged.add(b.get(m++));
      }
      else {
        merged.add(a.get(n++));
      }
    }
    
    while(n < a.size()) {
      merged.add(a.get(n++));
    }
    while(m < b.size()) {
      merged.add(b.get(m++));
    }
    
    return merged;
  }
}
