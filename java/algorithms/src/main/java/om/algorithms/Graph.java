package om.algorithms;

import java.util.ArrayDeque;

public class Graph {
  public static <T> void floodFill(T[][] area, T oldValue, T newValue, int x, int y) {
    int[] point = { x, y };
    
    ArrayDeque<int[]> stack = new ArrayDeque<int[]>();
    
    if(area[point[0]][point[1]] == oldValue) {
      stack.addFirst(point);
    }
    
    while(!stack.isEmpty()) {
      point = stack.pollFirst();
      // paint the point
      area[point[0]][point[1]] = newValue;
      
      //Check west
      if((point[1] > 0) && (area[point[0]][point[1] - 1] == oldValue)) {
        stack.addFirst(new int[] { point[0] , point[1] - 1});
      }
      //Check north
      if((point[0] > 0) && (area[point[0] - 1][point[1]] == oldValue)) {
        stack.addFirst(new int[] { point[0] - 1 , point[1]});
      }
      //Check east 
      if((point[1] < (area[0].length - 1)) && (area[point[0]][point[1] + 1] == oldValue)) {
        stack.addFirst(new int[] { point[0] , point[1] + 1});
      }
      //Check south 
      if((point[0] < (area.length - 1)) && (area[point[0] + 1][point[1]] == oldValue)) {
        stack.addFirst(new int[] { point[0] + 1 , point[1] });
      }
    }
  }
  
  public static <T> void floodFill2(T[][] area, T oldValue, T newValue, int x, int y) {
    int[] point = { x, y };
    
    ArrayDeque<int[]> stack = new ArrayDeque<int[]>();
    
    if(area[point[0]][point[1]] == oldValue) {
      stack.addFirst(point);
    }
    
    while(!stack.isEmpty()) {
      point = stack.pollFirst();
      // paint the point
      //area[point[0]][point[1]] = newValue;
      
      //Walk all the way west
      walk(area, oldValue, newValue, point[0], point[1], -1, stack, true);
      // and then all the way east
      walk(area, oldValue, newValue, point[0], point[1] + 1, 1, stack, true);
    }
  }
  
  private static <T> void walk(T[][] area, T oldValue, T newValue, int x, int y, int direction, ArrayDeque<int[]> stack, boolean push) {
    // The initial push is configurable to allow walking to one direction first
    // The other option would be to found a boundary first, e.g. east, and then walk west as far as possible
    // However, that would require in the worst that all nodes touched twice
    boolean pushNorth = push, pushSouth = push;
    
    // Let's walk west as far as we can
    // We check both boundaries, so we can walk in any direction
    while((y >= 0) && (y < area[x].length) && (area[x][y] == oldValue)) {
      
      // Let's see if we have a south candidate to be pushed on the stack
      if((x < (area.length - 1)) && (area[x + 1][y] == oldValue)) {
        // We have a candidate, let's see if we actually need to push it on the stack
        if(pushSouth) {
          stack.addFirst(new int[] { x + 1 , y });
          pushSouth = false;
        }
      }
      else {
        // if we still walking, we need to push the next candidate
        pushSouth = true;
      }
    
      // Let's see if we have a north candidate. The same code as above. There should be a way to generalize
      if((x > 0) && (area[x - 1][y] == oldValue)) {
        // We have a candidate, let's see if we actually need to push it on the stack
        if(pushNorth) {
          stack.addFirst(new int[] { x - 1 , y });
          pushNorth = false;
        }
      }
      else {
        // if we still walking, we need to push the next candidate
        pushNorth = true;

      }

      // Let's paint the node and check the next one
      area[x][y] = newValue;
      y += direction;
    }
  }
}