package om.algorithms;

import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;

public class Combinatoric {
  public static <T> List<List<T>> combinations(Collection<T> set, int k) {
    int[] counters = new int[k];
    ArrayList<List<T>> tuples = new ArrayList<List<T>>();
    ArrayList<T> tuple;
    ArrayList<T> elements = new ArrayList<T>(new HashSet<T>(set));
    
    // this is how we start with the counters for each position
    for(int i = 0; i < k; ++i) {
      counters[i] = i;
    }
    
    //while(counters[0] < (set.size() - (k - 1))) {
    boolean goOn = true;
    do {
      tuple = new ArrayList<T>();
      for(int e:counters) {
        tuple.add(elements.get(e));
      }
      tuples.add(tuple);
      goOn = !increment(counters, elements.size(), 0, 0);
//      goOn = !increment(counters, elements.size());
    } while(goOn);
    
    return tuples;
  }

  // This actually performs better than the iterative version despite 
  // creating a lot of extra arrays.
  public static <T> List<List<T>> combinations2(Collection<T> set, int k) {
    return combinations2(new ArrayList<T>(new HashSet<T>(set)), k - 1, k);
  }

  private static <T> List<List<T>> combinations2(List<T> set, int start, int k) {
    ArrayList<List<T>> tuples= new ArrayList<List<T>>();
    List<List<T>> subTuples;
    ArrayList<T> tuple;
    
    if(start > 0) {
      subTuples = combinations2(set, start - 1, k);
      int i  = start;
      int iStart = start;
      for(List<T> l: subTuples) {
        if(i >= set.size()) {
          ++iStart;
          i = iStart;
        }
        for(int j = i; j < set.size() - (k - 1 - start); ++j) {
          tuple = new ArrayList<T>(l);
          tuple.add(set.get(j));
          tuples.add(tuple);
        }
        ++i;
      }
    }
    else {
      for(int j = 0; j < set.size() - (k - 1); ++j) {
        tuple = new ArrayList<T>();
        tuple.add(set.get(j));
        tuples.add(tuple);
      }
    }
    
    return tuples;
  }
   
  public static <T> List<List<T>> permutations(Collection<T> set, int k) {
    List<List<T>> tuples = new ArrayList<List<T>>();
    // We start with all the combinations and then permutate each of them
    List<List<T>> combi = combinations2(set, k);
    
    for(List<T> tuple:combi) {
      tuples.addAll(permutate(tuple));
    }
   
    return tuples;
  }
  
  private static <T> List<List<T>> permutate(List<T> elements) {
    ArrayList<List<T>> tuples = new ArrayList<List<T>>();
    List<List<T>> subTuples;
    List<T> tuple;
    ArrayList<T> tmp = new ArrayList<T>(elements);
    
    if(elements.size() > 1) {
      for(T element:elements) {
        // take the current element out
        tmp.remove(element);
        subTuples = permutate(tmp);
        for(List<T> subTuple:subTuples) {
          tuple = new ArrayList<T>();
          tuple.add(element);
          tuple.addAll(subTuple);
          tuples.add(tuple);
        }
        // add the current element back in for next iteration
        tmp.add(element);
      }
          
    }
    else {
      for(T element:elements) {
        tuples.add(Arrays.asList(element));
      }
    }
    
    return tuples;
  }
  
  
  private static boolean increment(int[] counters, int n, int digit, int reset) {
    boolean overflow = true;
    boolean willOverflow = true;
    
    // We recurse through all the digits and provide a possible reset value. If we
    // reach digits + 1, the stack will unwind, starting with a carryOver. That will 
    // cause the least significant digit to increase. Everything unfolds from there.
    if(digit < counters.length) {
      // Check if this digit will actually trigger a carry over if incremented.
      // If so, we have to pass reset + 1 to the next digit as the reset. Otherwise
      // we pass the value of this digit + 1 as reset
      willOverflow = counters[digit] >= (n - counters.length + digit);
      overflow = increment(counters, n, digit + 1, willOverflow ? reset + 1 : counters[digit] + 2);
      if(overflow) {
        // If digit to the right carried over, so we will have to update this digit
        // accordingly.
        counters[digit] = willOverflow ? reset: counters[digit] + 1;
      }
    }
    
    return overflow && willOverflow;
  }
  

  // Reall not used at this point. The recursive version is more elegant
  private static boolean increment(int[] counters, int n) {
    // think about the counters as digits, where you increment a digit
    // the difference here is that when there is a carry over, we refill 
    // the digits to the right based on the most significant digit that has been 
    // increased. 
    boolean carryOver = true;
    int digit = counters.length - 1;
   
    do {
      // This one was a hard one for me. Depending on the digit, we count to a particular
      // index, before we get an overflow. For the last digit we count to n - 1
      if((counters[digit] >= (n - counters.length + digit))) {
        --digit;
      }
      else {
        ++counters[digit];
        carryOver = false;
      }
    } while(carryOver && (digit >= 0));
    
    // If the carry over flag is set, it means that we counted through all the possible
    // numbers and we skip updating the counter, b/c it is no longer needed
    if(!carryOver) {
      for(int i = digit + 1; i < counters.length; ++i) {
        counters[i] = counters[i - 1] + 1;
      }
    }
    
    // return if we had an overflow
    return carryOver;
  }
     
 
 
}
