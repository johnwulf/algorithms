package om.algorithms;

//import java.lang.invoke.MethodHandle;
//import java.lang.invoke.MethodHandles;
//import java.lang.invoke.MethodType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Random;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SortingTest {
  static ArrayList<Integer> random;
  static ArrayList<Integer> ascending;
  static ArrayList<Integer> descending;
  static ArrayList<Integer> duplicates;
  
  @BeforeClass
  public void beforeClass() {
    int l = 1000;
    int dup = 25;
    Random r = new Random(1034);
   
    random = new ArrayList<Integer>(l);
    ascending = new ArrayList<Integer>(l);
    descending = new ArrayList<Integer>(l);
    duplicates = new ArrayList<Integer>(l);
    
    for(int i = 0; i < l; ++i) {
      random.add(r.nextInt(100000));
      ascending.add(i);
      descending.add(l - i);
    }
    
    for(int i = 0; i < l; ++i) {
      duplicates.add(r.nextInt(l / dup));
    }
  }

  @DataProvider
  public Object[][] sortMethods()  {
    return new Object[][] {
      new Object[] { "Bubble Sort with Random", "bubbleSort", random },
      new Object[] { "Bubble Sort with Ascending", "bubbleSort", ascending },
      new Object[] { "Bubble Sort with Descending", "bubbleSort", descending },
      new Object[] { "Bubble Sort with Duplicate", "bubbleSort", duplicates },
      new Object[] { "Selection Sort with Random", "selectionSort", random },
      new Object[] { "Selection Sort with Ascending", "selectionSort", ascending },
      new Object[] { "Selection Sort with Descending", "selectionSort", descending },
      new Object[] { "Selection Sort with Duplicate", "selectionSort", duplicates },
      new Object[] { "Insertion Sort with Random", "insertionSort", random },
      new Object[] { "Insertion Sort with Ascending", "insertionSort", ascending },
      new Object[] { "Insertion Sort with Descending", "insertionSort", descending },
      new Object[] { "Insertion Sort with Duplicate", "insertionSort", duplicates },
      new Object[] { "Insertion Sort 2 with Random", "insertionSort2", random },
      new Object[] { "Insertion Sort 2 with Ascending", "insertionSort2", ascending },
      new Object[] { "Insertion Sort 2 with Descending", "insertionSort2", descending },
      new Object[] { "Insertion Sort 2 with Duplicate", "insertionSort2", duplicates },  
      new Object[] { "Merge Sort with Random", "mergeSort", random },
      new Object[] { "Merge Sort with Ascending", "mergeSort", ascending },
      new Object[] { "Merge Sort with Descending", "mergeSort", descending },
      new Object[] { "Merge Sort with Duplicate", "mergeSort", duplicates },    
      new Object[] { "Quick Sort with Random", "quickSort", random },
      new Object[] { "Quick Sort with Ascending", "quickSort", ascending },
      new Object[] { "Quick Sort with Descending", "quickSort", descending },
      new Object[] { "Quick Sort with Duplicate", "quickSort", duplicates },    
     };
  }

  @SuppressWarnings("unchecked")
  @Test(dataProvider="sortMethods")
  public void testSort(String name, String sort, @SuppressWarnings("rawtypes") Collection list) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException{
    checkArray((ArrayList<Integer>)getSortMethod(sort).invoke(null, list), name);
  }
  
  private Method getSortMethod(String name) throws NoSuchMethodException, IllegalAccessException {
    return Sorting.class.getMethod(name, Collection.class);
  }

  private void checkArray(ArrayList<Integer> a, String name) {
    for(int i = 1; i < a.size(); ++i) {
      assertThat("Order violated for " + name + " @ " + i, a.get(i - 1), is(lessThanOrEqualTo(a.get(i))));
    }
  }
}
