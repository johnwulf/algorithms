package om.algorithms;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import static org.testng.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Arrays;
import java.util.Set;

public class CombinatoricTest {
  @SuppressWarnings("unchecked")
  @DataProvider
  public Object[][] permutateDP() {
    return new Object[][] {
      new Object[] { 1, Arrays.asList ( 1, 2, 3, 4, 5 ),
                        new HashSet<List<Integer>> (Arrays.asList(
                          Arrays.asList(1), 
                          Arrays.asList(2),
                          Arrays.asList(3),
                          Arrays.asList(4),
                          Arrays.asList(5)
                        ))},
      new Object[] { 3, Arrays.asList ( 1, 2, 3 ),
                        new HashSet<List<Integer>> (Arrays.asList(
                          Arrays.asList(1, 2, 3),
                          Arrays.asList(1, 3, 2),
                          Arrays.asList(2, 1, 3),
                          Arrays.asList(2, 3, 1),
                          Arrays.asList(3, 2, 1),
                          Arrays.asList(3, 1, 2)
                        ))},
       new Object[] { 2, Arrays.asList( 1, 2, 3, 4, 5 ),
                        new HashSet<List<Integer>>(Arrays.asList(
                          Arrays.asList(1, 2),
                          Arrays.asList(2, 1),
                          Arrays.asList(1, 3),
                          Arrays.asList(3, 1),
                          Arrays.asList(1, 4),
                          Arrays.asList(4, 1),
                          Arrays.asList(1, 5),
                          Arrays.asList(5, 1),
                          Arrays.asList(2, 3),
                          Arrays.asList(3, 2),
                          Arrays.asList(2, 4),
                          Arrays.asList(4, 2),
                          Arrays.asList(2, 5),
                          Arrays.asList(5, 2),
                          Arrays.asList(3, 4),
                          Arrays.asList(4, 3),
                          Arrays.asList(3, 5),
                          Arrays.asList(5, 3),
                          Arrays.asList(4, 5),
                          Arrays.asList(5, 4)
                        ))},
    };
  }
  
  @SuppressWarnings("unchecked")
  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, Arrays.asList ( 1, 2, 3, 4, 5 ),
                        new HashSet<Set<Integer>> (Arrays.asList(
                          new HashSet<Integer>(Arrays.asList(1)), 
                          new HashSet<Integer>(Arrays.asList(2)),
                          new HashSet<Integer>(Arrays.asList(3)),
                          new HashSet<Integer>(Arrays.asList(4)),
                          new HashSet<Integer>(Arrays.asList(5))
                        ))},
      new Object[] { 5, Arrays.asList ( 1, 2, 3, 4, 5 ),
                        new HashSet<Set<Integer>> (Arrays.asList(
                          new HashSet<Integer>(Arrays.asList(1, 2, 3, 4, 5))
                        ))},
       new Object[] { 2, Arrays.asList( 1, 2, 3, 4, 5 ),
                        new HashSet<Set<Integer>>(Arrays.asList(
                          new HashSet<Integer>(Arrays.asList(1, 2 )),
                          new HashSet<Integer>(Arrays.asList(1, 3 )),
                          new HashSet<Integer>(Arrays.asList(1, 4 )),
                          new HashSet<Integer>(Arrays.asList(1, 5)),
                          new HashSet<Integer>(Arrays.asList(2, 3)),
                          new HashSet<Integer>(Arrays.asList(2, 4)),
                          new HashSet<Integer>(Arrays.asList(2, 5)),
                          new HashSet<Integer>(Arrays.asList(3, 4)),
                          new HashSet<Integer>(Arrays.asList(3, 5)),
                          new HashSet<Integer>(Arrays.asList(4, 5))
                        ))},
      new Object[] { 3, Arrays.asList( 1, 2, 3, 4, 5 ),
                        new HashSet<Set<Integer>>( Arrays.asList(
                          new HashSet<Integer>(Arrays.asList(1, 2, 3 )),
                          new HashSet<Integer>(Arrays.asList(1, 2, 4 )),
                          new HashSet<Integer>(Arrays.asList(1, 2, 5 )),
                          new HashSet<Integer>(Arrays.asList(1, 3, 4 )),
                          new HashSet<Integer>(Arrays.asList(1, 3, 5 )),
                          new HashSet<Integer>(Arrays.asList(1, 4, 5 )),
                          new HashSet<Integer>(Arrays.asList(2, 3, 4)),
                          new HashSet<Integer>(Arrays.asList(2, 3, 5)),
                          new HashSet<Integer>(Arrays.asList(2, 4, 5)),
                          new HashSet<Integer>(Arrays.asList(3, 4, 5))
                        ))},
      };
  }

  @Test(dataProvider = "permutateDP")
  public void permutates(int k, List<Integer> elements, Set<List<Integer>> expected) {
    List<List<Integer>> result = Combinatoric.permutations(elements, k);
    
    Set<List<Integer>> resultSet = new HashSet<List<Integer>>();
    for(List<Integer> tuple:result) {
      resultSet.add(tuple);
    }
    
    assertEquals(resultSet, expected, "testing for k=" + k);
  }
  
  @Test(dataProvider = "dp")
  public void combinations(int k, List<Integer> set, Set<Set<Integer>> expected) {
    // Create the combinations
    List<List<Integer>> result = Combinatoric.combinations(set, k);
    
    // Wrap the combinations and the tuples into a set for easy comparison
    Set<Set<Integer>> resultSet = new HashSet<Set<Integer>>();
    for(List<Integer> tuple:result) {
      resultSet.add(new HashSet<Integer>(tuple));
    }
    
    assertEquals(resultSet, expected, "testing for k=" + k);
  }
  
  @Test(dataProvider = "dp")
  public void combinations2(int k, List<Integer> set, Set<Set<Integer>> expected) {
    // Create the combinations
    List<List<Integer>> result = Combinatoric.combinations2(set, k);
    
    // Wrap the combinations and the tuples into a set for easy comparison
    Set<Set<Integer>> resultSet = new HashSet<Set<Integer>>();
    for(List<Integer> tuple:result) {
      resultSet.add(new HashSet<Integer>(tuple));
    }
    
    assertEquals(resultSet, expected, "testing for k=" + k);
  }
}
