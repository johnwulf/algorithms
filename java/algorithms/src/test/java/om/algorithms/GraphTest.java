package om.algorithms;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import static org.testng.Assert.*;

public class GraphTest {
  private static Integer[][] area1 = new Integer[][] {
          { 10, 10, 10, 10, 10, 10, 10, 10, 10},
          { 10, 11, 11, 11, 11, 10, 11, 11, 10},
          { 11, 10, 10, 10, 10, 10, 10, 11, 10},
          { 11, 10, 11, 11, 11, 11, 10, 11, 10},
          { 11, 10, 11, 10, 10, 10, 11, 11, 10},
          { 11, 10, 10, 11, 10, 11, 11, 10, 10},
          { 10, 11, 10, 11, 11, 10, 11, 10, 10},
          { 10, 11, 10, 10, 10, 10, 11, 10, 10},
          { 10, 11, 11, 11, 11, 11, 11, 11, 10},
          { 10, 10, 10, 10, 10, 10, 10, 10, 10}};
  
   private static Integer[][] area1Expected = new Integer[][] {
          { 20, 20, 20, 20, 20, 20, 20, 20, 20},
          { 20, 11, 11, 11, 11, 20, 11, 11, 20},
          { 11, 20, 20, 20, 20, 20, 20, 11, 20},
          { 11, 20, 11, 11, 11, 11, 20, 11, 20},
          { 11, 20, 11, 10, 10, 10, 11, 11, 20},
          { 11, 20, 20, 11, 10, 11, 11, 20, 20},
          { 20, 11, 20, 11, 11, 20, 11, 20, 20},
          { 20, 11, 20, 20, 20, 20, 11, 20, 20},
          { 20, 11, 11, 11, 11, 11, 11, 11, 20},
          { 20, 20, 20, 20, 20, 20, 20, 20, 20}};

   private static Integer[][] area1Expected2 = new Integer[][] {
          { 10, 10, 10, 10, 10, 10, 10, 10, 10},
          { 10, 11, 11, 11, 11, 10, 11, 11, 10},
          { 11, 10, 10, 10, 10, 10, 10, 11, 10},
          { 11, 10, 11, 11, 11, 11, 10, 11, 10},
          { 11, 10, 11, 20, 20, 20, 11, 11, 10},
          { 11, 10, 10, 11, 20, 11, 11, 10, 10},
          { 10, 11, 10, 11, 11, 10, 11, 10, 10},
          { 10, 11, 10, 10, 10, 10, 11, 10, 10},
          { 10, 11, 11, 11, 11, 11, 11, 11, 10},
          { 10, 10, 10, 10, 10, 10, 10, 10, 10}};
  
   @DataProvider
  public Object[][] ffDP() {
    return new Object[][] {
      new Object[] { 10, 20, 0, 0, area1, area1Expected },
      new Object[] { 10, 20, 9, 0, area1, area1Expected },
      new Object[] { 10, 20, 9, 8, area1, area1Expected },
      new Object[] { 10, 20, 7, 4, area1, area1Expected },
      new Object[] { 10, 20, 4, 4, area1, area1Expected2 },
      new Object[] { 10, 20, 1, 1, area1, area1 },
      
    };
  }

  @Test(dataProvider = "ffDP")
  public void floodFillTest(Integer oldValue, Integer newValue, int x, int y, Integer[][] area, Integer[][] expected)  {
    Integer[][] areaCopy = copyArray(area);
    
    Graph.floodFill(areaCopy, oldValue, newValue, x, y);
    
    assertEquals(areaCopy, expected);
  }

  @Test(dataProvider = "ffDP")
  public void floodFill2Test(Integer oldValue, Integer newValue, int x, int y, Integer[][] area, Integer[][] expected)  {
    Integer[][] areaCopy = copyArray(area);
    
    Graph.floodFill2(areaCopy, oldValue, newValue, x, y);
    
    assertEquals(areaCopy, expected);
  }
  
  private Integer[][] copyArray(Integer[][] src) {
    Integer[][] dest = new Integer[src.length][src[0].length];
    
    for(int i = 0; i < src.length; ++i) {
      System.arraycopy(src[i], 0, dest[i], 0, src[i].length);
    }
    
    return dest;
  }
      
      

 }
