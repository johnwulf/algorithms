package om.algorithms.ds;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import static org.testng.Assert.*;

import java.util.ArrayList;
import java.util.Random;


public class HeapTest {
  private Heap<Integer> simpleHeap1000;
  private ArrayList<Integer> random100;
  
  @BeforeClass
  public void setupTests() {
    ArrayList<Integer> list = new ArrayList<Integer>();
    
    for(int i = 0; i < 1000; i++) {
      list.add(i);
    }
    
    simpleHeap1000 = new Heap<Integer>(list, false);
    
    Random r = new Random(1000);
    
    random100 = new ArrayList<Integer>();
    for(int i = 0; i < 100; ++i) {
      random100.add(Integer.valueOf(r.nextInt(1000)));
    }
  }
  
  @DataProvider
  public Object[][] buildHeapDP() {
    return new Object[][] {
        new Object[] { 1, true },
        new Object[] { 3, true },
        new Object[] { 100, true },
        new Object[] { 127, true }, 
        new Object[] { 128, true }, 
        new Object[] { 129, true },
        new Object[] { 200, false }, 
    };
  }

  @DataProvider
  public Object[][] childDP() {
    return new Object[][] {
      new Object[] { simpleHeap1000, 0, -1  },
      new Object[] { simpleHeap1000, 1, 2  },
      new Object[] { simpleHeap1000, 2, 4  },
      new Object[] { simpleHeap1000, 8, 16 },
      new Object[] { simpleHeap1000, 800, 0 },
      new Object[] { simpleHeap1000, 2000, -1 }
    };
  }
  
  @DataProvider
  public Object[][] parentDP() {
    return new Object[][] {
      new Object[] { simpleHeap1000, 0, -1  },
      new Object[] { simpleHeap1000, 1, 0  },
      new Object[] { simpleHeap1000, 2, 1  },
      new Object[] { simpleHeap1000, 3, 1  },
      new Object[] { simpleHeap1000, 16, 8 },
      new Object[] { simpleHeap1000, 17, 8 },
      new Object[] { simpleHeap1000, 2000, -1 }
    };
  }
  
  @Test
  public void testInsert() {
    Heap<Integer> heap = new Heap<Integer>(random100, true);
    
    int x = heap.get(1);
   
    heap.insert(Integer.valueOf(x + 100));
    assertEquals(heap.get(1), Integer.valueOf(x + 100));
  }
  
  @Test
  public void testSortAscending() {
    Heap<Integer> heap = new Heap<Integer>(random100, true);
    while(heap.size() > 1) {
      heap.switchRoot();
    }
    
    checkSorted(heap);
  }
  
  @Test
  public void testSortDescending() {
    
    Heap<Integer> heap = new Heap<Integer>(random100, false);
    while(heap.size() > 1) {
      heap.switchRoot();
    }
    
    checkSorted(heap);
  }
  
  private void checkSorted(Heap<Integer> heap) {
    Integer[] array = new Integer[random100.size()];
    
    array = heap.toArrayAll(array);
    int last = heap.get(1);
    for(int e:array) {
      boolean inOrder;
      
      if(heap.isMaxHeap()) {
        inOrder = e >= last;
      }
      else {
        inOrder = e <= last;
      }
    
      if(!inOrder) {
        fail("Order violated");
        return;
      }
      last = e;
    }
  }
  
  @Test(dataProvider = "buildHeapDP")
  public void testBuildHeap(int size, boolean maxHeap) {
    Integer[] array = new Integer[size];
    
    for(int i = 0; i < size; i++) {
      if(maxHeap) {
        array[i] = Integer.valueOf(i * 3);
      }
      else {
        array[i] = Integer.valueOf(20000 - i * 5);
      }
    }
    
    Heap<Integer> heap = new Heap<Integer>(array, maxHeap);
    assertTrue(checkHeap(heap, 1, maxHeap));
  }
  
  
  public <T extends Object & Comparable<? super T>> boolean checkHeap(Heap<T> heap, int index, boolean maxHeap) {
    boolean isHeap = true;
    int left, right;
   
    left = heap.leftChildIndex(index);
    right = heap.rightChildIndex(index);
    if(left > 0) {
      if(maxHeap) {
        if(heap.get(index).compareTo(heap.get(left)) < 0) {
          return false;
        }
      }
      else {
        if(heap.get(left).compareTo(heap.get(index)) < 0) {
          return false;
        }
      }
      isHeap = checkHeap(heap, left, maxHeap);
    }
    
    if(isHeap && (right > 0)) {
      if(maxHeap) {
        if(heap.get(index).compareTo(heap.get(right)) < 0) {
          return false;
        }
      }
      else {
        if(heap.get(right).compareTo(heap.get(index)) < 0) {
          return false;
        }
      }
      isHeap = checkHeap(heap, right, maxHeap);
    }
    
    return isHeap;
  }
    
  @Test(dataProvider = "childDP")
  public void testLeftChild(Heap<Integer> heap, int parent, int child) {
    assertEquals(Integer.valueOf(heap.leftChildIndex(parent)), Integer.valueOf(child));
  }

  @Test(dataProvider = "parentDP")
  public void parent(Heap<Integer> heap, int child, int parent) {
    assertEquals(heap.parentIndex(child), parent);
  }

  @Test(dataProvider = "childDP")
  public void testRightChild(Heap<Integer> heap, int parent, int child) {
    assertEquals(Integer.valueOf(heap.rightChildIndex(parent)), Integer.valueOf(child <= 0 ? child : (child + 1)));
  }
  
}
