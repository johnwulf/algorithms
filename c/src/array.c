#include <stdio.h>
#include <stdlib.h>
#include "array.h"

void print_array(array_t *arr);
int array_error =0;

array_t * alloc_array(){
  array_t * array = malloc(sizeof(array_t));
  if(array){
    array->elements = calloc(ARRAY_DEFAULT_SIZE, sizeof(unsigned int));
    if(array->elements){
      array->number_of_elements = 0;
      array->size = ARRAY_DEFAULT_SIZE;
    }
    else{
      free(array);
      array = NULL;
    }
  }
  return array;
}

void free_array(array_t * a){
  if(a){
    if(a->elements){
      free(a->elements);
    }
    free(a);
  }
}

unsigned long insert_at(array_t *a, unsigned long position, unsigned int element){
  /*
   * walk to slot n
   * copy all elements from n to end of array to slot n+1 to end of array +1
   * insert element at slot n
   * */
  if(is_valid(a)){
    resize(a, position);

    int i;
    /*
    memmove(position * sizeof(unsigned int), (position + 1) * sizeof(unsigned int), 
       (a->number_of_elements - position) * sizeof(unsigned int));
   */

    for(i=a->number_of_elements+1; i > position; i--){
      a->elements[i] = a->elements[i-1];
    }
    a->elements[position] = element;
    a->number_of_elements++;
    return position;
  }else{
    return -1;
  }
}

int resize(array_t *a, unsigned long position){
  if(position >= a->size){
    unsigned int *tmp_elements = a->elements;
    unsigned long new_size = position + ARRAY_DEFAULT_SIZE;
    a->elements = calloc(new_size, sizeof(unsigned int));
    if(a->elements){

      /*
      memccpy(a->elements, tmp_elements, a->number_of_elements, sizeof(unsigned int));
      */

      int i=0;
      for(i=0; i<a->number_of_elements; i++){
        a->elements[i] = tmp_elements[i];
      }
      free(tmp_elements);
      a->size = new_size;
      return 0;
    }
    else {
      return -1;
    }
  }
}

void set_element(array_t *a, unsigned long position, unsigned int element){
  if (is_valid(a)){
    resize(a, position);
  }
}

int is_valid(array_t *a) {
  return a && a->elements;
}

unsigned int element_at(array_t *a, unsigned long position){
  if(is_valid(a)){
    if(position <= a->number_of_elements){
      return a->elements[position];
    }else{
       array_error = ARRAY_INDEX_OUT_OF_BOUNDS_ERROR;
       return array_error;
    }
  }
}

unsigned int remove_at(array_t *a, unsigned long position){
  array_error = ARRAY_NO_ERROR;
  printf("Array error is: %d\n", array_error);
  if(is_valid(a)){
    if(position <= a->number_of_elements){
      int x = a->elements[position];
      long i;
      for(i=position; i < a->number_of_elements; i++){
        a->elements[i] = a->elements[i+1];
      }
      printf("i is: %lu \n", i);
      a->elements[i-1] = 9999;
      return x;
    }else{
      array_error = ARRAY_INDEX_OUT_OF_BOUNDS_ERROR;
      return array_error;
    }
  }
}

unsigned long length(array_t *a){
	return 0L;
}


void print_array(array_t *arr) {
  int i;

  printf("%lu:", arr->number_of_elements);
  for(i=0; i<arr->number_of_elements; i++){
    printf("[%u]", arr->elements[i]);
  }

  printf("\n");
}
