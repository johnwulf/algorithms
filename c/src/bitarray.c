#include "bitarray.h"
#include <stdlib.h>

bitarray_t* alloc_bitarray(long size) {
	bitarray_t *bitarray = NULL;

	bitarray = malloc(sizeof(bitarray_t));
	if(bitarray) {	

		bitarray->size = 0;
		bitarray->set = NULL;

		if(size > 0) {
			bitarray->set = (bitarray_word_t *)calloc(BITARRAY_SIZE(size), sizeof(bitarray_word_t));
			bitarray->size = bitarray->set ? size : 0;
		}
	}

	return bitarray;
}

void free_bitarray(bitarray_t **bitarray) {
	if(bitarray && (*bitarray)) {
		if((*bitarray)->set) {
			free((*bitarray)->set);
			free(*bitarray);
			bitarray = NULL;
		}
	}
}

void clear_bitarray(bitarray_t *bitarray) {
	int i;

	if((bitarray->size > 0) && bitarray->set) {
		for(i = 0; i < BITARRAY_SIZE(bitarray->size); i++) {
			bitarray->set[i] = 0;
		}
	}
}

/*!
 * Helper function to check the index boundary and 
 * enable negative indizes. The index has to be within
 * -bitarray.size <= i < bitarray.size. A valid negative 
 * index is interpreted from the end of the bitarray, i.e.
 * the index -1 yields the last bit in the bitarray, -2
 * the second last and so on.
 *
 * @param[in] bitarray a bitarray structure
 * @param[in] i the index to be checked and adjusted.
 *
 * @return the proper index of the bit or -1 if the given
 *         index was not valid.
 */
long check_and_adjust_index(bitarray_t *bitarray, long i) {
	if(bitarray->set && (i < bitarray->size) && (i >= -bitarray->size)) {
		return i < 0 ? bitarray->size - i : i;
	}

	return -1;
}

int is_bit_set(bitarray_t *bitarray, long i) {
	long index = check_and_adjust_index(bitarray, i);
	if(index >= 0) {
		return (bitarray->set[WORD(index)] & BIT(index)) != 0;
	}

	return 0;
}

void set_bit(bitarray_t *bitarray, long i) {
  long index = check_and_adjust_index(bitarray, i);
	if(index >= 0) {
		bitarray->set[WORD(i)] |= BIT(i);
	}
}

void clear_bit(bitarray_t *bitarray, long i) {
	long index = check_and_adjust_index(bitarray, i);
	if(index >= 0) {
  	bitarray->set[WORD(i)] &= (~BIT(i));
	}
}

