#include <stdio.h>
#include <bitarray.h>
#include <CUnit/CUnit.h>
#include <CUnit/Console.h>


CU_pSuite setup_bitarray_suite(void);

int initialize_test() {
	printf("Initializing before tests will be run");
	return 0;
}

int cleanup_test() {
	printf("Cleaning up the mess the tests left befind");
	return 0;
}

void test_test() {
	printf("Executing a test");
	CU_FAIL("Failing the test");
}

int main(int argc, char** argv) {
	printf("Test for algorithms library\n");
	CU_initialize_registry();
	
	CU_pSuite bitarray_suite = setup_bitarray_suite();

	CU_console_run_tests();
}

