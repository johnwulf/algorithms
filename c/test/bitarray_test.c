#include <stdio.h>
#include "bitarray.h"
#include <CUnit/CUnit.h>

bitarray_t* one_word_bitarray;
bitarray_t* large_bitarray;
bitarray_t* invalid_bitarray;

#define BUF_SIZE 255
char buf[BUF_SIZE];
int buf_size = BUF_SIZE;

int initialize_bitarray_test() {
	one_word_bitarray = alloc_bitarray(8 * sizeof(bitarray_word_t));
	large_bitarray = alloc_bitarray(1000);

	return 0;
}

int cleanup_bitarray_test() {
	free_bitarray(&one_word_bitarray);
	free_bitarray(&large_bitarray);
	return 0;
}

void test_creation() {
	bitarray_t *bitarray = alloc_bitarray(100);
	int i;
	int v = 0;

	CU_ASSERT_EQUAL(bitarray->size, 100);
	for(i = 0; i < 100; i++) {
		v |= is_bit_set(bitarray, i);
	}
	CU_ASSERT_FALSE(v);

	free_bitarray(&bitarray);
}

void test_destroy() {
	bitarray_t *bitarray = alloc_bitarray(100);

	free_bitarray(&bitarray);
	CU_ASSERT_PTR_NULL(bitarray);

	//CU_ASSERT_EQUAL(bitarray.size, 0);
	//CU_ASSERT_FALSE(bitarray.set);
}

void test_set_general(bitarray_t *bitarray, int max) {
	int i;

	CU_ASSERT_EQUAL(bitarray->size, max);
	CU_ASSERT_FALSE(is_bit_set(bitarray, max - 1));

	for(i = 0; i < bitarray->size; i += 2) {
		set_bit(bitarray, i);
	}

	int e = -1, u = 0, c = 0;

	for(i = 0; i < bitarray->size; i += 2) {
		c++;
		e &= is_bit_set(bitarray, i);
		u |= is_bit_set(bitarray, i + 1);
	}

	CU_ASSERT_EQUAL(c, max / 2);
	CU_ASSERT_TRUE(e);
	CU_ASSERT_FALSE(u);

	clear_bitarray(bitarray);
	for(i = 0; i < bitarray->size; i += 2) {
		set_bit(bitarray, i + 1);
	}

	e = 0;
	u = -1;
 	c = 0;

	for(i = 0; i < bitarray->size; i += 2) {
		c++;
		e |= is_bit_set(bitarray, i);
		if(is_bit_set(bitarray, i)) {
			printf("Even bit %i was set when it shouldn't", i);
		}
		if(!is_bit_set(bitarray, i + 1)) {
			printf("Odd bit %i was set when it shouldn't", i + 1);
		}	
		u &= is_bit_set(bitarray, i + 1);
	}

	CU_ASSERT_EQUAL(c, max / 2);
	CU_ASSERT_TRUE(u);
	CU_ASSERT_FALSE(e);
}

void test_one_word_bitarray() {
	test_set_general(one_word_bitarray, sizeof(bitarray_word_t) * 8);
}

void test_large_bitarray() {
	test_set_general(large_bitarray, large_bitarray->size);
}

void test_set_bit() {
	bitarray_t *bitarray = alloc_bitarray(100);

	set_bit(bitarray, 10);

	CU_ASSERT_EQUAL(bitarray->set[0], 1024L);

	set_bit(bitarray, 10 + 8 * 2 * sizeof(bitarray_word_t));

	CU_ASSERT_EQUAL(bitarray->set[0], 1024L);
	free_bitarray(&bitarray);
}

void test_bit_word() {
	bitarray_word_t bit;
	long word;
	
	bit = BIT(10);
  word = WORD(10);

	CU_ASSERT_EQUAL(bit, 1024L);
	CU_ASSERT_EQUAL(word, 0);

	bit = BIT(10 + 8 * 2 * sizeof(bitarray_word_t));
	word = WORD(10 + 8 * 2 * sizeof(bitarray_word_t));

	CU_ASSERT_EQUAL(bit, 1024);
	CU_ASSERT_EQUAL(word, 2);
}

void test_bitarray_size() {
	int word_size = sizeof(bitarray_word_t) * 8;

	CU_ASSERT_EQUAL(BITARRAY_SIZE(word_size), 1);
	CU_ASSERT_EQUAL(BITARRAY_SIZE(word_size - 1), 1);
	CU_ASSERT_EQUAL(BITARRAY_SIZE(word_size + 1), 2);
	CU_ASSERT_EQUAL(BITARRAY_SIZE(word_size * 2), 2);
	CU_ASSERT_EQUAL(BITARRAY_SIZE(word_size + 20), 2);
}


void test_clear() {
	int i;

	for(i = 0; i < large_bitarray->size; i++) {
		set_bit(large_bitarray, i);
	}

	clear_bitarray(large_bitarray);

	int b = 0;
	for(i = 0; i < large_bitarray->size; i++) {
		b |= is_bit_set(large_bitarray, i);
	}

	CU_ASSERT_FALSE(b);
}

CU_pSuite setup_bitarray_suite() {
	CU_pSuite suite = CU_add_suite("Bitset", initialize_bitarray_test, cleanup_bitarray_test);

	CU_ADD_TEST(suite, test_creation);
	CU_ADD_TEST(suite, test_destroy);
	CU_ADD_TEST(suite, test_bit_word);
	CU_ADD_TEST(suite, test_set_bit);
	CU_ADD_TEST(suite, test_one_word_bitarray);
	CU_ADD_TEST(suite, test_large_bitarray);
	CU_ADD_TEST(suite, test_clear);
	CU_ADD_TEST(suite, test_bitarray_size);
}
	
