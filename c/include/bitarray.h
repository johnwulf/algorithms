#ifndef __bitarray_H__
#define __bitarray_H__

typedef unsigned long bitarray_word_t; 

typedef struct bitarray {
	long size;
	bitarray_word_t *set;
} bitarray_t;

#define BITARRAY_SIZE(i) (((i) + (sizeof(bitarray_word_t) << 3) - 1) / (sizeof(bitarray_word_t) << 3))
#define BIT(i) (((bitarray_word_t) 1) << ((i) % (sizeof(bitarray_word_t) << 3)))
#define WORD(i) ((i) / (sizeof(bitarray_word_t) << 3))

bitarray_t* alloc_bitarray(long i);
void free_bitarray(bitarray_t **bitarray);

int is_bit_set(bitarray_t *bitarray, long i);
void set_bit(bitarray_t *bitarray, long i);
void clear_bit(bitarray_t *bitarray, long i);

void clear_bitarray(bitarray_t *bitarray);
#endif
