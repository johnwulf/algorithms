#ifndef __ARRAY_H__
#define __ARRAY_H__

#define ARRAY_DEFAULT_SIZE 100

extern int array_error;

enum array_error_codes {
  ARRAY_NO_ERROR = 0,
  ARRAY_INDEX_OUT_OF_BOUNDS_ERROR
};

typedef struct { 
  unsigned int *elements;
  unsigned long size;
  unsigned long number_of_elements;
} array_t;

array_t * alloc_array();
void free_array(array_t *);
unsigned long insert_at(array_t *, unsigned long, unsigned int);
unsigned int element_at(array_t *, unsigned long);
unsigned int remove_at(array_t *, unsigned long);
unsigned long length(array_t *);
void set_element_at(array_t *, unsigned long, unsigned int);

#endif
